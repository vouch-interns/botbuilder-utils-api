## Vouch BotBuilder Utilities

This project aims to increase the effectiveness and intuition of Bot building, through the use of APIs to handle common functions.

1. Reformatting of single-line variants, for POS integration. (URL/api/parseVariant)
2. Process User's last name input against guest name in POS system, to verify if User is registered. (URL/api/processName)
3. Split string based on delimiter. (URL/api/splitString)
4. Returns the length of a valid array object (URL/api/arrayLength)

This technologies used in this project are NodeJS and ExpressJS. Live URL at https://murmuring-sierra-54650.herokuapp.com/
