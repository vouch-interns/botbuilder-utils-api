const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const stringRoutes = require('./routes/string')

// -----App Init-----
const app = express()
const port = process.env.PORT || 4000

// -----Middlewares Init-----
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// -----Route Handlers-----
app.get('/', (req, res) => res.send('Hello World!'))
app.use('/api', stringRoutes)

// -----Server Handler-----
app.listen(port, () => {
  console.log(
    `Vouch Botbuilder Utils App listening at http://localhost:${port}`,
  )
})
