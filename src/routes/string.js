const express = require('express')
const router = express.Router()

const {
  parseVariant,
  processName,
  splitString,
  arrayLength,
} = require('../controllers/string')

router.post('/parseVariant', parseVariant)
router.post('/processName', processName)
router.post('/splitString', splitString)
router.post('/arrayLength', arrayLength)

module.exports = router
