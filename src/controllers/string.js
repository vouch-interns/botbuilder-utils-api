exports.parseVariant = (req, res) => {
  const { input } = req.body
  if (!input) {
    return res.json({ data: '' })
  }
  const variantArray = input.split(',')
  const variantArrayLength = variantArray.length
  return res.json({ data: { array: variantArray, length: variantArrayLength } })
}

exports.processName = (req, res) => {
  const { input, record } = req.body
  if (!input) {
    return res.json({ data: '' })
  }
  const recordArray = record.split(' ').map((word) => word.toLowerCase())
  if (recordArray.includes(input.toLowerCase())) {
    return res.json({ data: 'true' })
  }
  return res.json({ data: 'false' })
}

exports.splitString = (req, res) => {
  const { input, delimiter } = req.body
  if (!input) {
    return res.json({ data: '' })
  }
  const inputArray = input.split(delimiter)
  return res.json({ data: inputArray })
}

exports.arrayLength = (req, res) => {
  const { input } = req.body
  if (!input) {
    return res.json({ data: 0 })
  }
  return res.json({ data: Math.floor(input.length / 16) })
}
